# XSL Conversion App running on Windows Server

XML file processing (transforming and tranfering according to the configuration):

1. Load configuration;
1. Poll the source dirctories (looping through the list of the source directories);
1. Loop through the new files;
1. Parse each file;
1. Find transformation and routing rule;
1. Load XSLT file (if not already chached);
1. Transform the parsed source file;
1. Transfer file to the destination;
1. If nothing (no file) was processed since the previous run, "sleep" for a while

## Changes

2.00    Added "chardet" module for encoding auto-detection and Windows event log source name registration